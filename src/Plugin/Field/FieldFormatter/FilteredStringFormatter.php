<?php

namespace Drupal\rocketship_embed_content_block\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'filtered_string' formatter.
 *
 * @FieldFormatter(
 *   id = "filtered_string",
 *   label = @Translation("Filtered text"),
 *   field_types = {
 *     "string_long"
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class FilteredStringFormatter extends StringFormatter {

  /**
   * @var \Drupal\filter\FilterFormatInterface[]
   */
  protected $formats;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $class = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $class->formats = filter_formats();
    return $class;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();
    $options['format'] = NULL;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $options = [];
    foreach ($this->formats as $format) {
      $options[$format->id()] = $format->label();
    }

    $form['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Format'),
      '#description' => $this->t('Select what text format should be used for rendering this field.'),
      '#default_value' => $this->getSetting('format'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if ($format = $this->getSetting('format')) {
      if (isset($this->formats[$format])) {
        $summary[] = $this->t('Format: @format', ['@format' => $this->formats[$format]->label()]);
      }
    }
    return $summary;
  }

  /**
   * Convert some style properties to html attributes.
   *
   * @param string $text
   *   The original string.
   *
   * @return string
   *   The processed string.
   */
  protected function convertStyleAttrToHtmlAttrs($text) {
    $html_dom = Html::load($text);
    $iframes = $html_dom->getElementsByTagName('iframe');
    foreach ($iframes as $iframe) {
      $style = $iframe->getAttribute('style');

      if (!$style) {
        continue;
      }

      // Get all css properties from the style attribute.
      $properties = explode(';', $style);
      $properties = array_map('trim', $properties);

      foreach ($properties as $property) {
        if (!$property) {
          continue;
        }

        // Get property name and value.
        [$property_name, $property_value] = explode(':', $property);

        if (!isset($property_name) || !isset($property_value)) {
          continue;
        }

        $property_name = trim($property_name);
        $property_value = trim($property_value);

        // Set up height and width as html attributes.
        if (in_array($property_name, ['height', 'width'])) {
          $iframe->setAttribute($property_name, $property_value);
        }
      }
    }

    return Html::serialize($html_dom);
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return array
   *   The textual output generated as a render array.
   */
  protected function viewValue(FieldItemInterface $item) {
    // Convert height and width from style attr to html attributes due style
    // attribute is not supported during filtering and will be removed from
    // the output.
    $text = $this->convertStyleAttrToHtmlAttrs($item->value);

    return [
      '#type' => 'processed_text',
      '#text' => $text,
      '#format' => isset($this->formats[$this->getSetting('format')]) ? $this->getSetting('format') : 'plain_text',
      '#langcode' => $item->getLangcode(),
    ];
  }

}
